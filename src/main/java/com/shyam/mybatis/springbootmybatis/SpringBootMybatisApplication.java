package com.shyam.mybatis.springbootmybatis;

import org.apache.ibatis.type.MappedTypes;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.shyam.mybatis.springbootmybatis.model.Param;
import com.shyam.mybatis.springbootmybatis.model.Param2;
import com.shyam.mybatis.springbootmybatis.model.State;
import com.shyam.mybatis.springbootmybatis.model.Users;

@MappedTypes({Users.class,Param.class,Param2.class,State.class})
@MapperScan("com.shyam.mybatis.springbootmybatis.mapper")
@SpringBootApplication
public class SpringBootMybatisApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootMybatisApplication.class, args);
	}
}
