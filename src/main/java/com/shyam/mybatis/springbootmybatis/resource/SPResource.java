package com.shyam.mybatis.springbootmybatis.resource;

import java.util.List; 

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shyam.mybatis.springbootmybatis.mapper.SPMapper;
import com.shyam.mybatis.springbootmybatis.model.Param;
import com.shyam.mybatis.springbootmybatis.model.Param2;
import com.shyam.mybatis.springbootmybatis.model.State;

@RestController
@RequestMapping("/rest/sp")
public class SPResource {

	private SPMapper sPMapper;

	public SPResource(SPMapper sPMapper) {
		this.sPMapper = sPMapper;
	}

	@GetMapping("/callGetTotalCity")
	public int callGetTotalCity() {
		Param param = new Param();
		sPMapper.callGetTotalCityAnnotations(param);
		System.out.println(param.getTotal());
		return param.getTotal();
	}

	@GetMapping("/getTotalCityByStateId")
	public int getTotalCityByStateId() {

		Param2 param2 = new Param2();
		param2.setStateId(1);
		sPMapper.callGetTotalCityStateIdAnnotations(param2);
		System.out.println(param2.getTotal());
		return param2.getTotal();
	}

	@GetMapping("/callGetStatesAnnotations")
	public List<State> callGetStatesAnnotations() {

		List<State> li = sPMapper.callGetStatesAnnotations();
		System.out.println(li);
		return li;
	}

}
