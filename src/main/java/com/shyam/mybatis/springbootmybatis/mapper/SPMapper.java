package com.shyam.mybatis.springbootmybatis.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.mapping.StatementType;

import com.shyam.mybatis.springbootmybatis.model.Param;
import com.shyam.mybatis.springbootmybatis.model.Param2;
import com.shyam.mybatis.springbootmybatis.model.State;

/**
 * Stored Procedure Mapper contains all the myBatis/iBatis annotations

 */
@Mapper
public interface SPMapper {

	Object callGetTotalCity(Param param);
	Integer callGetTotalCity2(Map<String, Object> param);
	Object callGetTotalCityStateId(Param2 param2);
	List<State> callGetStates();
	
	//--methos bellow are the same as above, but they are using annotations
	
	@Select(value= "{ CALL getTotalCity( #{total, mode=OUT, jdbcType=INTEGER} )}")
	@Options(statementType = StatementType.CALLABLE)
	Object callGetTotalCityAnnotations(Param param); 
	
	@Select(value= "{ CALL getTotalCityStateId( #{stateId, mode=IN, jdbcType=INTEGER}, #{total, mode=OUT, jdbcType=INTEGER})}")
	@Options(statementType = StatementType.CALLABLE)
	Object callGetTotalCityStateIdAnnotations(Param2 param2);
	
	//TODO: set resultMap with annotations
	@Select(value= "{ CALL getStates()}")
	@Options(statementType = StatementType.CALLABLE)
	@Results(id="test",value = {
		@Result(property="id", column="state_id"),
		@Result(property="name", column="state_name"),
		@Result(property="code", column="state_code")
	})
	List<State> callGetStatesAnnotations();
}
