package com.shyam.mybatis.springbootmybatis.model;

/**
 * Param2 POJO
 */
public class Param2 {

	private int total;
	private int stateId;
	
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public int getStateId() {
		return stateId;
	}
	public void setStateId(int stateId) {
		this.stateId = stateId;
	}
}
