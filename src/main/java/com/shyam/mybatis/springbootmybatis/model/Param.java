package com.shyam.mybatis.springbootmybatis.model;

/**
 * Param POJO
 * 
 */
public class Param {

	private int total;

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}
}
